## SA-SHAGO: SA-SHA with Global Optimization
Simple front-end that extracts matchable from RGB-D data and builds a
g2o graph.

### How to use the system
Here you can find the steps to follow to build this package on your system.
##### Prerequisites
Here are reported the Ubuntu packages required to build this system.
1. Install basic Ubuntu packages
```bash
sudo apt-get install build-essential libeigen3-dev libsuitesparse-dev freeglut3-dev libqglviewer-dev libyaml-cpp-dev
```
2. Install ROS (tested only on `ROS Kinetic`). Follow the instructions available [here](http://wiki.ros.org/kinetic/Installation/Ubuntu)
3. Install `catkin` build tools
```bash
sudo apt-get install python-catkin-tools ninja-build
```

##### Building SA-SHAGO
Once that all those packages are correctly installed, it is possible to download the dependencies of this project and build the resulting workspace. The required packes are the following:

* [srrg_cmake_modules](https://gitlab.com/srrg-software/srrg_cmake_modules) - collection of cmake modules (must be updated to commit [5dd5ead9](https://gitlab.com/srrg-software/srrg_cmake_modules/tree/5dd5ead90f578dea4696bc56bb82bb3eda9ea471) or newer)
* [srrg_core](https://gitlab.com/srrg-software/srrg_core) - low-level algorithms and utilities
* [srrg_core_viewers](https://gitlab.com/srrg-software/srrg_core_viewers) - simple viewer based on QT
* [srrg_gl_helpers](https://gitlab.com/srrg-software/srrg_gl_helpers) - low-level openGL utilities
* [srrg_hbst](https://gitlab.com/srrg-software/srrg_hbst) - lightweight header-only library for binary descriptor-based VPR
* [g2o](https://github.com/RainerKuemmerle/g2o) - graph optimization framework
* [srrg_g2o_chordal_plugin](https://gitlab.com/srrg-software/srrg_g2o_chordal_plugin) - g2o plugin for 3D pose graph optimization
* [srrg_g2o_matchable_plugin](https://gitlab.com/srrg-software/srrg_g2o_matchable_plugin) - g2o plugin for matchable optimization

The steps to set-up everything are the following
1. Install `g2o`. Follow the instructions [here](https://github.com/RainerKuemmerle/g2o). If you already have `g2o` installed, make sure that the version is equal or newer than [79623a8](https://github.com/RainerKuemmerle/g2o/commit/79623a840f3e9ee3936c9adaec53cdd7778faffc). You should alse set the `$G2O_ROOT` environment variable and copy the `$G2O_ROOT/build/g2o/config.h` file in `$G2O_ROOT/g2o/config.h`
3. Install our `g2o chordal plugin`. Follow the instructions [here](https://gitlab.com/srrg-software/srrg_g2o_chordal_plugin)
4. Install our `g2o matchable plugin`. Follow the instructions [here](https://gitlab.com/srrg-software/srrg_g2o_matchable_plugin).
5. Download the remaining packages in your workspace
```bash
cd catkin_ws/src  # navigate to the catkin workspace
git clone https://gitlab.com/srrg-software/srrg_cmake_modules
git clone https://gitlab.com/srrg-software/srrg_core
git clone https://gitlab.com/srrg-software/srrg_core_viewers
git clone https://gitlab.com/srrg-software/srrg_gl_helpers
git clone https://gitlab.com/srrg-software/srrg_hbst
```
6. Download this package in your workspace and build everything
```bash
cd catkin_ws/src  # navigate to the catkin workspace
git clone https://gitlab.com/srrg-software/srrg_sashago
cd ..         # return to catkin workspace root
catkin build  # build all packages
```

##### Run SA-SHAGO
The main app works with `txtio` data files. You can convert your own RGB-D bag using the provided `srrg_core` tool.
You can run this app
```bash
rosrun srrg_core_ros srrg_message_dumper_node -t <depth-topic> -t <rgb-topic> -o <output>.txtio
```
and then play the bag you want to convert.
```bash
rosbag play <path-to-your-bag>.bag
```

The system requires a yaml configuration file in which are specified the topic names and other system parameters. Standard configurations are provided in the `configurations` folder.
To run the main SA-SHAGO app, simply type
```bash
cd <txtio-dataset-root-folder>
rosrun srrg_sashago sashago_txtio_app <path-to-configuration>.yaml <dataset>.txtio
```

We provide also two app to tune the configuration for the detector and the loop closer submodules.

### Authors
* [Irvin Aloise](https://istinj.github.io/)
* [Bartolomeo Della Corte](http://bartdc.gitlab.io/)
* [Federico Nardi](https://www.dis.uniroma1.it/~dottoratoii/students/306)
* [Giorgio Grisetti](https://sites.google.com/dis.uniroma1.it/grisetti)

### Related publications
Please cite our most recent article if you use our software:
```
@article{aloise2019systematic,
  title={Systematic Handling of Heterogeneous Geometric Primitives in Graph-SLAM Optimization},
  author={Aloise, Irvin and Della Corte, Bartolomeo and Nardi, Federico and Grisetti, Giorgio},
  journal={IEEE Robotics and Automation Letters},
  volume={4},
  number={3},
  pages={2738--2745},
  year={2019},
  publisher={IEEE}
  doi={10.1109/LRA.2019.2918054}
}
```

### Something is not working?
Open an [issue](https://gitlab.com/srrg-software/srrg_sashago/issues/new) or contact the package maintainers.

### License
BSD-4 License
