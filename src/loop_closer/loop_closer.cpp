#include <loop_closer/loop_closer.h>

#include <g2o/plugin_matchables/matchable_types/types_matchables.h>
#include <g2o/plugin_se3_chordal/types_chordal3d/types_chordal3d.h>

namespace srrg_sashago {

  LoopCloser::LoopCloser() {
    _optimizer = new g2o::SparseOptimizer();
  }

  LoopCloser::~LoopCloser() {
    delete _optimizer;
    _optimizer = 0;
  }

  void LoopCloser::compute() {
    if (!_current_scene || !_map)
      throw std::runtime_error(
        "[BagashaLoopCloser::compute]| please set the current scene and the map");

    if (!_closure_constraints.size() || !_matching_scene)
      return;

    // ia this now is not possible because our ass does not work with 2 scenes only
    // 0. get association from hbst
    // 1. solve with the aligner
    // 2. get T

    //    //ia TODO we do an edge
    //    //ia initial guess of the transform between the scenes
    //    const Isometry3 lc_meas = _matching_scene->pose().inverse() * _current_scene->pose();
    //
    //    g2o::EdgeSE3Chord* e = new g2o::EdgeSE3Chord();
    //    e->vertices()[0] = _matching_scene->poseVertex();
    //    e->vertices()[1] = _current_scene->poseVertex();
    //    e->setInformation(Matrix12::Identity().cast<number_t>()); //ia this is a shit
    //    e->setMeasurement(lc_meas.cast<number_t>());
    //    _optimizer->addEdge(e);

    size_t num_merged = 0;
    for (size_t c = 0; c < _closure_constraints.size(); ++c) {
      SceneEntry* s_match = _closure_constraints[c].fixed;
      SceneEntry* s_query = _closure_constraints[c].moving;

      // ia check if there are landmarks in this contraint
      if (!s_match->landmark() || !s_query->landmark()) {
        continue;
      }

      // ia both were already landmarks
      if (s_match->landmark() == s_query->landmark()) {
        //        if (_config.verbosity == VerbosityLevel::Debug) {
        //          std::cerr << "[BagashaLoopCloser::compute]| skipping same landmark compa'" <<
        //          std::endl;
        //        }
        continue;
      }

      _optimizer->mergeVertices(
        s_match->landmark()->vertexPtr(), s_query->landmark()->vertexPtr(), true);
      _map->mergeLandmarks(s_match->landmark(), s_query->landmark());
      ++num_merged;
    }

    if (_config.verbosity == VerbosityLevel::Debug) {
      std::cerr << "[BagashaLoopCloser::compute]| number of merged landmarks = " << num_merged
                << std::endl;
    }
  }

  void LoopCloser::performBatchOptimization() {
    if (!_scenes)
      throw std::runtime_error("[BagashaLoopCloser::performBatchOptimization]| invalid scene "
                               "container, did you forget to call setSceneContainer?");

    // ia create a solver for the chordal optimizer
    std::unique_ptr<SlamLinearSolverCholmod> linear_solver =
      g2o::make_unique<SlamLinearSolverCholmod>();
    linear_solver->setBlockOrdering(true);
    std::unique_ptr<SlamBlockSolver> block_solver =
      g2o::make_unique<SlamBlockSolver>(std::move(linear_solver));
    g2o::OptimizationAlgorithmGaussNewton* solver =
      new g2o::OptimizationAlgorithmGaussNewton(std::move(block_solver));
    if (!solver)
      throw std::runtime_error("[BagashaLoopCloser::performBatchOptimization]| impossible to "
                               "create optimization algoritm");
    _optimizer->setAlgorithm(solver);

    // ia we assume that the gauge is already specified, to avoid that g2o fixes different vertices
    // ia between the two graphs
    if (_optimizer->gaugeFreedom())
      throw std::runtime_error("[BagashaLoopCloser::performBatchOptimization]| selected graph has "
                               "no fixed vertices, exit");

    // ia robust kernels, if present
    if (_config.optimization_kernel != "" && _config.use_robust_kernel) {
      if (_config.verbosity == VerbosityLevel::Debug) {
        std::cerr << "[BagashaLoopCloser::performBatchOptimization]| robust kernel: "
                  << _config.optimization_kernel << std::endl;
      }

      _factory_kernel = g2o::RobustKernelFactory::instance()->creator(_config.optimization_kernel);
      if (!_factory_kernel)
        throw std::runtime_error(
          "[BagashaLoopCloser::performBatchOptimization]| unknown kernel type type");

      g2o::SparseOptimizer::EdgeSet::iterator edge_it   = _optimizer->edges().begin();
      g2o::SparseOptimizer::EdgeSet::iterator edges_end = _optimizer->edges().end();
      while (edge_it != edges_end) {
        g2o::SparseOptimizer::Edge* e = dynamic_cast<g2o::SparseOptimizer::Edge*>(*edge_it);
        e->setRobustKernel(_factory_kernel->construct());
        e->robustKernel()->setDelta(_config.optimization_kernel_width);
        ++edge_it;
      }
    }

    // ia initialize the shit and optimize
    if (_config.verbosity == VerbosityLevel::Debug)
      _optimizer->setVerbose(true);
    else
      _optimizer->setVerbose(false);

    _optimizer->initializeOptimization();
    _optimizer->computeActiveErrors();
    double initial_chi2     = _optimizer->activeChi2();
    int optimization_result = _optimizer->optimize(_config.num_iterations);
    if (_config.num_iterations > 0 && optimization_result == g2o::OptimizationAlgorithm::Fail)
      std::cerr << "[BagashaLoopCloser::performBatchOptimization]| "
                << FG_YELLOW("Cholesky failed, result may be invalid") << std::endl;

    if (_config.verbosity == VerbosityLevel::Debug) {
      std::cerr << "[BagashaLoopCloser::performBatchOptimization]| initial chi2 = " << initial_chi2
                << "\t final chi2 = " << _optimizer->activeChi2() << std::endl;
    }

    // ia update all the poses
    for (IntSceneMap::iterator s_it = _scenes->begin(); s_it != _scenes->end(); ++s_it) {
      const Isometry3 updated_T =
        dynamic_cast<g2o::VertexSE3EulerPert*>(s_it->second->poseVertex())->estimate().cast<real>();
      s_it->second->setPose(updated_T);
    }
  }

} // namespace srrg_sashago
