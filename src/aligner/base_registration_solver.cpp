#include <limits>
#include "base_registration_solver.h"

namespace srrg_sashago {
  
  const BaseRegistrationSolver::SelectionMatrixEntry BaseRegistrationSolver::_selection_matrix[][SELECTION_MATRIX_DIM]=
  {
    {
      {// point-point
       .compute_p=true,
       .compute_d=false,
       .compute_o=false,
       .compute_x=false,
       .rotate_omega=false
      },
      {// point-line
       .compute_p=true,
       .compute_d=false,
       .compute_o=false,
       .compute_x=false,
       .rotate_omega=false
      },
      {// point-plane
       .compute_p=true,
       .compute_d=false,
       .compute_o=false,
       .compute_x=false,
       .rotate_omega=false
      }
    },
    {
      {// line-point
       .compute_p=true,
       .compute_d=false,
       .compute_o=false,
       .compute_x=false,
       .rotate_omega=true
      },
      {// line-line
       .compute_p=true,
       .compute_d=true,
       .compute_o=false,
       .compute_x=false,
       .rotate_omega=false
      },
      {// line-plane
       .compute_p=true,
       .compute_d=false,
       .compute_o=true,
       .compute_x=false,
       .rotate_omega=false
      }
    },
    {
      {// plane-point
       .compute_p=true,
       .compute_d=false,
       .compute_o=false,
       .compute_x=false,
       .rotate_omega=true
      },
      {// plane-line
       .compute_p=true,
       .compute_d=false,
       .compute_o=true,
       .compute_x=false,
       .rotate_omega=true
      },
      {// plane-plane
       .compute_p=true,
       .compute_d=true,
       .compute_o=false,
       .compute_x=false,
       .rotate_omega=false
      }
    }
  };

  BaseRegistrationSolver::BaseRegistrationSolver() {
    setOmegaDirection(1);
    setOmegaOrthogonal(1);
    setOmegaIntersection(1);
    setDamping(0.1);
    _point_kernel_threshold        = std::numeric_limits<real>::max();
    _direction_kernel_threshold    = std::numeric_limits<real>::max();
    _orthogonal_kernel_threshold   = std::numeric_limits<real>::max();
    _intersection_kernel_threshold = std::numeric_limits<real>::max();
    _ignore_outliers=false;
    _T.setIdentity();
  }

  BaseRegistrationSolver::~BaseRegistrationSolver() {
    _upper_flag = true;
  }

  void BaseRegistrationSolver::init(const Isometry3& transform_,
                                    const bool use_support_weight) {
    _T=transform_;
    _use_support_weight = use_support_weight;
  }
  

} //ia end namespace srrg_bagasha
