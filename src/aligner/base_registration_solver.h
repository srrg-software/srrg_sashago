#pragma once
#include "types/scene.h"
#include "types/constraint.h"

#define SELECTION_MATRIX_DIM 3

namespace srrg_sashago {

  class BaseRegistrationSolver {
    public:
      // constructs an empty solver
      BaseRegistrationSolver();

      // destroys the solver
      virtual ~BaseRegistrationSolver();

      // prepares the optimization, starting from the specified pose
      virtual void init(const Isometry3& transform_ = Isometry3::Identity(),
                        const bool use_support_weight = false);

      // executes one round of optimization
      virtual void oneRound() = 0;

      //ia this is a shit
      // accessor to the vector of constraints
      inline const ConstraintVector& constraints() const { return _constraints; }
      inline ConstraintVector& constraints() {return _constraints;}

      // the current transform
      inline const Isometry3& transform() const {return _T;}
      inline void setTransform(const Isometry3& transform_= Isometry3::Identity()) {
        _T=transform_;
      }


      // damping parameter. Higher damping slower but more conservative convergence
      inline const real& damping() const {return _damping;}
      inline void setDamping(const real& damping_) {_damping=damping_;}

      // sets the weight of the direction factor
      // higher values result in more importance given to plane normals
      // and line directions
      inline const real& omegaDirection() const {return _omega_d_scalar;}
      inline void setOmegaDirection(const real& omega_d_scalar)  {
        _omega_d_scalar=omega_d_scalar;
        _omega_d=Eigen::Matrix3f::Identity()*_omega_d_scalar;
      }

      // sets the weight for the line-plane containment
      // higher value results in this class of constraints more satisfied than others
      inline const real& omegaOrthogonal() const {return _omega_o;}
      inline void setOmegaOrthogonal(const real& omega_o)  { _omega_o = omega_o;}

      // weight for the intersection constraints
      inline const real& omegaIntersection() const {return _omega_x;}
      inline void setOmegaIntersection(const real& omega_x)  { _omega_x = omega_x;}


      // threshold for point-point/ point-line / point-plane distances
      inline const real& pointKernelThreshold() const { return _point_kernel_threshold;}
      inline void setPointKernelThreshold(const real& threshold)  { _point_kernel_threshold=threshold;}

      // threshold for the direction vector of planes-plane and line-line distances
      inline const real& directionKernelThreshold() const { return _direction_kernel_threshold;}
      inline void setDirectionKernelThreshold(const real& threshold)  { _direction_kernel_threshold=threshold;}

      // threshold for line-plane direction orthogonality
      inline const real& orthogonalKernelThreshold() const { return _orthogonal_kernel_threshold;}
      inline void setOrthogonalKernelThreshold(const real& threshold)  { _orthogonal_kernel_threshold=threshold;}

      // threshold for line-line intersection
      inline const real& intersectionKernelThreshold() const { return _intersection_kernel_threshold;}
      inline void setIntersectionKernelThreshold(const real& threshold)  { _intersection_kernel_threshold=threshold;}

      // if set to true outliers (avove kernels) are ignored
      inline const bool ignoreOutliers() const {return _ignore_outliers;}
      inline void setIgnoreOutliers(const bool ignore_outliers)  { _ignore_outliers=ignore_outliers; }

      inline const bool& cheCazzoDevoFare() {return _upper_flag;}

    protected:


      //ia sort of configuration ??
      ConstraintVector _constraints;         //< constraint vector
      Isometry3 _T;                  //< transform
      Matrix3 _omega_d;                      //< temp spherical omega for direction vectors

      real _omega_d_scalar;                 //< spherical direction omega magnutude
      real _omega_o;                        //< omega of orthogonality (line-plane)
      real _omega_x;                        //< omega of intersection
      real _damping;                        //< damping (for milder convergence)
      real _point_kernel_threshold;         //< threshold for point error
      real _direction_kernel_threshold;     //< threshold for direction error
      real _orthogonal_kernel_threshold;    //< threshold for orthogonality error (plane-line)
      real _intersection_kernel_threshold;  //< threshold for intersection
      bool _use_support_weight;              //< if true, use the contraint weights
      bool _ignore_outliers;                 //< if true, outliers are ignored in optimization

      bool _upper_flag;

      //! entry of action matrix that tells what to do with a specific constraint
      struct SelectionMatrixEntry{
          bool compute_p;
          bool compute_d;
          bool compute_o;
          bool compute_x;
          bool rotate_omega;
          bool reverse;
      };

      static const SelectionMatrixEntry _selection_matrix[][SELECTION_MATRIX_DIM];
    public:
      EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  };
  
} //ia end namespace srrg_bagasha
