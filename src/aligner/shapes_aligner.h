#pragma once
#include <srrg_system_utils/system_utils.h>

#include "direct_registration_solver.h"
#include "iterative_registration_solver.h"
#include "nn_correspondence_finder.h"

namespace srrg_sashago {

  class ShapesAligner{
  public:

    struct Config {
      VerbosityLevel verbosity;

      size_t max_iterations;
      bool use_direct_solver;
      bool use_support_weight;

      Config() {
        verbosity = VerbosityLevel::Info;

        max_iterations = 10;
        use_direct_solver = false;
        use_support_weight = false;
      }
    };

    struct TimeStats {
      double data_association;
      double solve;

      TimeStats() {
        setZero();
      }

      inline void setZero() {
        data_association = 0.0;
        solve = 0.0;
      }

      friend std::ostream& operator <<(std::ostream& os_, const TimeStats& stats_) {
        os_ << "[ShapesAligner::TimeStats]| data_ass = " << stats_.data_association
            << "\tsolve=" << stats_.solve;
        return os_;
      }
    };

    //! @brief ctor/dtor
    ShapesAligner();
    virtual ~ShapesAligner();

    //! @brief inline set/get methods
    inline const Config& config() const {return _configuration;}
    inline Config& mutableConfig() {return _configuration;}
    inline const TimeStats& timeStats() const {return _time_stats;}

    inline void setFixedMap(WorldMap* fixed_map_){_map_fixed = fixed_map_;}
    inline void setMovingScene(Scene* scene_moving_){_scene_moving = scene_moving_;}
    inline void setT(const Isometry3& T_){_T = T_;}
    inline const Isometry3& T() const {return _T;}

    void compute(const Isometry3& init_guess_ = Isometry3::Identity());

    inline BaseRegistrationSolver& solver(){
      if(_configuration.use_direct_solver) return _direct_solver; return _iterative_solver;}
    NNCorrespondenceFinder* nnCorrespondenceFinder() const {return _ass;}

  protected:
    void _computeDirect(const Isometry3& init_guess_ = Isometry3::Identity());
    void _computeIterative(const Isometry3& init_guess_ = Isometry3::Identity());

    Config _configuration;
    TimeStats _time_stats;

    WorldMap* _map_fixed;
    Scene* _scene_moving;
    Isometry3 _T;

    //ia modules in the config
    NNCorrespondenceFinder*     _ass = 0;
    IterativeRegistrationSolver _iterative_solver;
    DirectRegistrationSolver    _direct_solver;

    TimeStats _stats;

  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  };


} //ia end namespace srrg_bagasha
