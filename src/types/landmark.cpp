#include "landmark.h"
#include <iostream>

namespace srrg_sashago {

  Landmark::Landmark(SceneEntry* entry_,
                     const size_t& id_) :
                         _id(id_) {
    _entry_ptr = entry_;
    _entry_ptr->_landmark_ptr = this;

    assert(!_entry_ptr->nextSceneEntry() && "[Landamark::Landmark] unexpected error");

    SceneEntry* e = _entry_ptr;
    while (e->_prev) {
      e->_prev->_landmark_ptr = this;
      e = e->_prev;
    }
    _origin = e;

    assert(_origin && "[Landamark::Landmark] invalid origin");
    assert(_origin->landmark() == this && "[Landamark::Landmark] unexpected error");
  }

  Landmark::~Landmark() {}

  void Landmark::merge(Landmark* landmark_) {
    assert(landmark_ && "[Landmark::merge]| invalid landmark");
    assert(landmark_ != this && "[Landmark::merge]| same landmark, exit");

    //ia update the track - connect last SceneEntry of this
    //ia with the origin of landmark_
    SceneEntry* landmark_entry = landmark_->origin();
    SceneEntry* this_last_entry = _entry_ptr;

//    std::cerr << "dest                       = " << (size_t)this << std::endl;
//    std::cerr << "source                     = " << (size_t)landmark_ << std::endl;
//    std::cerr << "source->origin->landmark() = " << (size_t)landmark_entry->landmark() << std::endl;

    assert(landmark_entry->landmark() != this && "[Landmark::merge]| your bookkeeping is wrong, exit");
    assert(landmark_entry->landmark() == landmark_ && "[Landmark::merge]| your bookkeeping is wrong, exit");

    while (this_last_entry->nextSceneEntry()) {
      this_last_entry = this_last_entry->nextSceneEntry();
    }

    assert(this_last_entry->landmark() == this && "[Landmark::merge]| landmark ptr mismatch");

    //ia reassign edges
    for (g2o::HyperGraph::Edge* e : landmark_->edges()) {
      _edges.insert(e);
    }

    //ia update age
    const size_t& age_offset = this_last_entry->_age + 1;
    this_last_entry->_next = landmark_entry;
    landmark_entry->_age += age_offset;

    //ia adjust the bookkeeping
    while(landmark_entry) {
      landmark_entry->_age += age_offset;
      landmark_entry->setLandmarkPtr(this);
      landmark_entry = landmark_entry->nextSceneEntry();
    }

  }

} /* namespace srrg_bagasha */
