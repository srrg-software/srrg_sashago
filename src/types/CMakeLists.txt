add_library(srrg_sashago_types_library SHARED
  defs.h
  matchable.h
  scene_entry.h
  scene_entry.cpp
  constraint.h
  landmark.h
  landmark.cpp
  scene.h
  scene.cpp
  world_map.h
  world_map.cpp)

#ia TODO modify this shit linking a minimal amount of library here
target_link_libraries(srrg_sashago_types_library 
  srrg_system_utils_library
  srrg_core_types_library
  ${catkin_LIBRARIES}
  ${OpenCV_LIBS})
