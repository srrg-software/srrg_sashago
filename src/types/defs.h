#pragma once
#include <iostream>
#include <stdexcept>
#include <Eigen/Geometry>

#include "shell_colors.h"

#define DOUBLE_BAR "======================================================================================================="
#define SINGLE_BAR "-------------------------------------------------------------------------------------------------------"

namespace srrg_sashago {

  typedef float real;

  enum VerbosityLevel {None=0x00, Info=0x01, Time=0x02, Debug=0xFF};

  template <typename Scalar_>
  using Rotation2_ = Eigen::RotationBase<Scalar_, 2>;

  template <typename Scalar_>
  using Quaternion_ = Eigen::Quaternion<Scalar_>;

  using Vector1 = Eigen::Matrix<real, 1, 1>;
  using Vector2 = Eigen::Matrix<real, 2, 1>;
  using Vector3 = Eigen::Matrix<real, 3, 1>;
  using Vector4 = Eigen::Matrix<real, 4, 1>;
  using Vector5 = Eigen::Matrix<real, 5, 1>;
  using Vector6 = Eigen::Matrix<real, 6, 1>;
  using Vector9 = Eigen::Matrix<real, 9, 1>;
  using Vector12 = Eigen::Matrix<real, 12, 1>;

  using Matrix1 = Eigen::Matrix<real, 1, 1>;
  using Matrix2 = Eigen::Matrix<real, 2, 2>;
  using Matrix3 = Eigen::Matrix<real, 3, 3>;
  using Matrix4 = Eigen::Matrix<real, 4, 4>;
  using Matrix5 = Eigen::Matrix<real, 5, 5>;
  using Matrix6 = Eigen::Matrix<real, 6, 6>;
  using Matrix9 = Eigen::Matrix<real, 9, 9>;
  using Matrix12 = Eigen::Matrix<real, 12, 12>;

  using Quaternion = Eigen::Quaternion<real>;
  using Rotation2 = Eigen::RotationBase<real, 2>;

  //ds TODO move?
  using Matrix3_4 = Eigen::Matrix<real, 3, 4>;
  using Matrix3_6 = Eigen::Matrix<real, 3, 6>;
  using Matrix3_9 = Eigen::Matrix<real, 3, 9>;
  using Matrix3_12 = Eigen::Matrix<real, 3, 12>;
  using Matrix4_6 = Eigen::Matrix<real, 4, 6>;
  using Matrix2_6 = Eigen::Matrix<real, 2, 3>;
  using Matrix1_3 = Eigen::Matrix<real, 1, 3>;
  using Matrix1_6 = Eigen::Matrix<real, 1, 6>;
  using Matrix1_12 = Eigen::Matrix<real, 1, 12>;
  using Matrix6_12 = Eigen::Matrix<real, 6, 12>;
  
  using MatrixX = Eigen::Matrix<real, Eigen::Dynamic, Eigen::Dynamic>;

  using Isometry2 = Eigen::Transform<real, 2, Eigen::Isometry>;
  using Isometry3 = Eigen::Transform<real, 3, Eigen::Isometry>;

  using AngleAxis = Eigen::AngleAxis<real>;


  //! @brief viewer colors
  #define VIEWER_COLOR_RED Eigen::Vector3f(0.8f,0.1f,0.1f)
  #define VIEWER_COLOR_BLUE Eigen::Vector3f(0.1f,0.1f,0.8f)
  #define VIEWER_COLOR_GREEN Eigen::Vector3f(0.1f,0.8f, 0.1f)
  #define VIEWER_COLOR_CYAN Eigen::Vector3f(0.55f, 0.89f, 0.93f)
  #define VIEWER_COLOR_VIOLET Eigen::Vector3f(0.67f, 0.36f, 0.90f)

  #define VIEWER_COLOR_DARK_RED Eigen::Vector3f(0.3f,0.0f,0.0f)
  #define VIEWER_COLOR_DARK_BLUE Eigen::Vector3f(0.0f,0.0f,0.3f)
  #define VIEWER_COLOR_DARK_GREEN Eigen::Vector3f(0.0f,0.3f, 0.0f)
  #define VIEWER_COLOR_DARK_CYAN Eigen::Vector3f(0.0f, 0.58f, 0.53f)

} //ia end namespace
