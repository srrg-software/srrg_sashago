#pragma once
#include "landmark.h"
#include "scene.h"

namespace srrg_sashago {

  //! @brief this is a local map actually :)
  class WorldMap {
  public:

    using LandmarkContainer = LandmarkSet;
    using EntryContainer = SceneEntryVector;

    struct Config {
      size_t age_threshold_point;
      size_t age_threshold_line;
      size_t age_threshold_plane;

      Config() {
        age_threshold_point = 8;  //inspired by Mirco Colosi
        age_threshold_line  = 8;  //inspired by Mirco Colosi
        age_threshold_plane = 20; //inspired by Mirco Colosi
      }
    };

    WorldMap();
    virtual ~WorldMap();

    //! @brief configuration
    const Config& config() const {return _configuration;}
    Config& mutableConfig() {return _configuration;}

    //! @brief inline get
    const LandmarkContainer& landmarks() const {return _landmarks;}
    const inline size_t numLandmarks() const {return _landmarks.size();}

    const EntryContainer& entries() const {return _entry_pool;}
    const inline size_t numEntries() const {return _entry_pool.size();}

    //! @brief clears the map and deletes the landmarks - the only thing that it owns
    void destroy();

    //! @brief adds a candidate entry into the pull;
    //!        if the entry is not old enough it returns 0, otherwise the created landmark
    Landmark* addEntry(SceneEntry* entry_);

    //! @brief merges two landmarks, removing source and assigning to dest all its
    //! scene entries vertices and edges.
    void mergeLandmarks(Landmark* dest_, Landmark* source_, bool erase_ = true);

    //! @brief adds to the pull all the landmark in the pull
    void updatePool();
    void clearPool();


    static void resetLandmarkIDGenerator() {
      landmark_id_generator = 0;
    }

  protected:
    //! @brief aux function: given a matchable checks if it is old enough
    //                       to be inserted in the map (aka the former matchable store)
    const bool _isOldEnough(SceneEntry* e_) const;

    EntryContainer _entry_pool;      //ia pull of scene entries. some of them are landmarks, some just candidate
    LandmarkContainer _landmarks;    //ia landmark container; a view of the entries_pull containing only landmarks

    Config _configuration;

    static size_t landmark_id_generator;
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  };

} //ia end namespace srrg_bagasha

