#include <relocalizer/relocalizer.h>

namespace srrg_sashago {
  
  Relocalizer::Relocalizer() {
  }

  Relocalizer::~Relocalizer() {
  }

  void Relocalizer::compute() {
    if (!_scene || !_scenes)
      throw std::runtime_error("[BAGASHA RELOCALIZER][compute]|please set the scene ptr and the scene container ptr");

    _constraints.clear();
    _matching_scene = 0;

    // bdc get entries of the scene
    const SceneEntryVector& entries = _scene->entries();
    const size_t num_entries = entries.size();

    // bdc hbst vector for ptrs and descriptors
    BinaryEntryVector binary_entry_vector(num_entries);
    
    // bdc populate the hbst vector
    size_t k = 0;
    for(SceneEntry* entry_ptr : entries) {
      if(entry_ptr->matchable().type() != Matchable::Type::Plane) {
        binary_entry_vector[k++] = new BinaryEntry(entry_ptr,
                                                   entry_ptr->descriptor(),
                                                   _scene->sceneID());
      }
    }

    binary_entry_vector.resize(k);
    
    // bdc quiry the tree
    Tree::MatchVectorMap matches;   
    _hbst_tree.matchAndAdd(binary_entry_vector, matches, _config.maximum_matching_distance);

    //ia skip if we do not have enough scenes yet
    if ((int)(_scenes->size() - _config.frame_interspace)  < 0)
      return;

    // bdc README
    // matches is a vector. Every element is the match_vector for every scene added til now.
    size_t num_matches = 0;
    size_t best_index = 0;
    real best_ratio = 0.0;

    // bdc from now on, we have in matches[best_index].second the correspondences, i.e. the
    // scene entries constraints
    //ia compute the score
    for (size_t match_idx = 0; match_idx < matches.size() - _config.frame_interspace; ++match_idx) {
      real current_ratio = (real)matches[match_idx].size() / k;
      if (current_ratio > best_ratio) {
        best_ratio = current_ratio;
        best_index = match_idx;
      }
    }
    
    num_matches = matches[best_index].size();

    if (_config.verbosity > VerbosityLevel::None) {
      std::cerr << "[BagashaRelocalizer::compute]| hbst matching scene = " << best_index
                << "\t num matches = "<< num_matches
                << "\t similarity score = " << best_ratio << std::endl;
    }

    //ia relocalization failure
    if (best_ratio < _config.min_relocalization_score ||
        num_matches < _config.min_relocalization_matches)
      return;

    // bdc given the num_matches and the distance btw this scene and the best_index one,
    // we can construct a vector of BaseSolver::Correspondences w/ the pointers
    _matching_scene = _scenes->at(best_index);

    _constraints.resize(matches[best_index].size());
    k = 0;
    for (size_t i = 0; i < matches[best_index].size(); ++i) {
      //ia if this query is associated with more than one guy, this means that is ambigous
      //ia thus, we discard this association.
      //if (matches[best_index][i].object_references.size() > 1)
      //  continue;
      //ia THANKS TO DOMINIK SCHLEGEL WE HAVE A NICE LINE OF CODE - Best Award 2018
      _constraints[k++] = Constraint(matches[best_index][i].object_references[0],
                                     matches[best_index][i].object_query);
    }
    _constraints.resize(k);
  }
  
} //ia namespace srrg_bagasha 
