#pragma once
#include <srrg_image_utils/depth_utils.h>
#include <srrg_types/defs.h>

#include <g2o/core/sparse_optimizer.h>
#include <g2o/plugin_matchables/matchable_types/types_matchables.h>
#include <g2o/plugin_se3_chordal/types_chordal3d/types_chordal3d.h>

#include "aligner/shapes_aligner.h"
#include "types/world_map.h"

namespace srrg_sashago {

  class ShapeTracker {
  public:
    // ia all the parameters and external modules are here
    struct Config {
      float point_distance_threshold;
      float direction_distance_threshold;
      float scene_percentage_for_break;
      float resolution;
      int age_threshold_point;
      int age_threshold_line;
      int age_threshold_plane;
      size_t maximum_matching_distance;

      bool recursive_edge_generation;

      VerbosityLevel verbosity;

      Config() {
        point_distance_threshold     = .1f; // merge only if the points are close
        direction_distance_threshold = .8f; // merge only if the directions are close
        scene_percentage_for_break   = .8f; // [0;1] break if N% of the scene is not matched
        resolution                   = 0.05f;
        age_threshold_point          = 8;  // inspired by Mirco Colosi in jail
        age_threshold_line           = 8;  // inspired by Mirco Colosi
        age_threshold_plane          = 20; // inspired by Mirco Colosi
        maximum_matching_distance    = 25; // hbst matching distance for tree 'quiry'

        // ia generate edges also with all the previous track of the landmark
        // iathis should be substituted with the actual merge of the landmark (computing its mean)
        recursive_edge_generation = false;

        verbosity = VerbosityLevel::Time;
      }
    };

    //! @brief ctor/dtor
    ShapeTracker();
    virtual ~ShapeTracker();

    // inline set/get methods
    inline const Config& config() const {
      return _configuration;
    }
    inline Config& mutableConfig() {
      return _configuration;
    }

    inline ShapesAligner* aligner() const {
      return _aligner;
    }

    inline const Isometry3& globalT() const {
      return _global_T;
    }
    inline const Isometry3& invGlobalT() const {
      return _inv_global_T;
    }

    inline void setOptimizer(g2o::SparseOptimizer* opt_) {
      _optimizer = opt_;
    }
    inline void setMap(WorldMap* map_) {
      _map = map_;
    }
    inline void setMovingScene(Scene* moving_) {
      _moving_scene = moving_;
    }

    //! @brief initializes something
    void init();

    //! @brief does the trick :)
    void compute(const Isometry3& guess_ = Isometry3::Identity());

    //! @brief resets the id generator of the vertices
    static void resetVertexIdGenerator() {
      _vertex_id_generator = 0;
    }

  protected:
    g2o::HyperGraph::Edge* _createPoseEdge(g2o::HyperGraph::Vertex* from_,
                                           g2o::HyperGraph::Vertex* to_);

    //! @brief aux function: merges the source_ matchable with the dest_ one
    //    void _mergeMatchables(Matchable* dest_, Matchable* source_);

    //! @brief aux function: generates a Matchable Vertex
    g2o::matchables::VertexMatchable* _generateMatchableVertex(const Matchable& m_);

    //! @brief aux function: generates a Pose-Matchable Edge
    g2o::HyperGraph::Edge* _generateMatchableEdge(g2o::HyperGraph::Vertex* pose_v_,
                                                  g2o::HyperGraph::Vertex* matchable_v,
                                                  const Matchable& matchable_meas_);

    //! @brief aux functions: compute a specific edge
    g2o::HyperGraph::Edge* _computePlaneEdge(g2o::VertexSE3EulerPert* vfrom_,
                                             g2o::matchables::VertexMatchable* vto_,
                                             const Matchable& m_);
    g2o::HyperGraph::Edge* _computeLineEdge(g2o::VertexSE3EulerPert* vfrom_,
                                            g2o::matchables::VertexMatchable* vto_,
                                            const Matchable& m_);
    g2o::HyperGraph::Edge* _computePointEdge(g2o::VertexSE3EulerPert* vfrom_,
                                             g2o::matchables::VertexMatchable* vto_,
                                             const Matchable& m_);

    //! @brief aligner module - owned
    ShapesAligner* _aligner = 0;

    //! @brief ptr of last created scene - not owned
    Scene* _moving_scene = 0;

    //! @brief ptr of map - not owned
    WorldMap* _map = 0;

    //! @brief ptr to the optimizer - not owned
    g2o::SparseOptimizer* _optimizer = 0;

    //! @brief ptr to the last created point vertex
    g2o::HyperGraph::Vertex* _prev_pose_vertex = 0;

    //! @brief the camera tranform
    Isometry3 _global_T;
    Isometry3 _inv_global_T;

    //! @brief configuration and misc things
    Config _configuration;

    static size_t _vertex_id_generator;

  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  };

} // namespace srrg_sashago
