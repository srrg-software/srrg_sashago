#include "system/sashago_system.h"

using namespace srrg_sashago;

int main(int argc, char **argv) {
  if (argc < 2)
    throw std::runtime_error("app usage: <exe> <configuration_output_file>");

  std::string configuration_filename = argv[1];

  SashagoSystem system;
  system.writeConfigurationToFile(configuration_filename);
  return 0;
}




